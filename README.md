## 初始化环境
```
yarn init 安装依赖
yarn dev  启动项目
yarn build 打包项目
```

## 项目目录
```
api         接口目录,主要就是axios的封装
assets      静态资源目录
common      公共目录
 |-base         基础的配置目录
 |-enums        公共的枚举目录
 |-types        公共的类型目录
components      组件目录
 |-elementplus  elementplus封装组件目录
 |-home         首页组件目录
 |-dept         部门组件目录
 |-station      岗位组件目录
 |-user         用户组件目录
 |-role         角色组件目录
 |-permission   权限组件目录
 |-menu         菜单组件目录
 |-permission   权限组件目录
 |-svgicon      svgicon组件目录(阿里iconfont支持svg组件)
 |-其他          业务组件目录
views       路由组件目录
router      路由目录
stores      pinia目录
utils       工具目录
```

## 环境基础搭建(开发人员不用关心)
```
1.安装vite
npm create vite@latest

2.创建vue项目
yarn create vite clark-vite-web
1. 选择vue
2. 选择自定义Customize with create-vue
3. 添加依赖
	3.1 ADD TS
	3.2 Add Router
	3.3 Add Pinia
	3.4 Add ESLint
	3.5 Prettier Code
4.进入项目 yarn install
5.启动项目 yarn dev
```
---
## 基础依赖
### 1. 安装pinia
```
yarn add pinia
在 main.ts中 
引人 pinia
import { createPinia } from 'pinia'
注册使用
app.use(createPinia())

pina持久化
yarn add pinia-plugin-persistedstate

在 main.ts中引人
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

```
### 2.安装element-plus
```
https://element-plus.org/zh-CN
yarn add element-plus

安装element-plus图标库
yarn add @element-plus/icons-vue
------------------------------------------------------------

在main.js 中配置
//引人element-plus
import ElementPlus from 'element-plus';
//引人element-plus样式
import 'element-plus/dist/index.css';
// 引入element-plus 字体图标库
import * as ElementPlusIconsVue from '@element-plus/icons-vue';
// 引入国际化
import zhCn from 'element-plus/es/locale/lang/zh-cn';
// 注册element-plus 字体图标库
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
//使用element-plus + 国际化
app.use(ElementPlus, { locale: zhCn });
----------------------------------------------------------

element-plus 支持TS
在tsconfig.app.json 中配置 element-plus/global，支持TS
"types": [
    "element-plus/global",
]
```
### 3.安装axios
```
yarn add axios
```

### 4.安装nprogress
```
yarn add nprogress
npm i --save-dev @types/nprogress
```

### 5.安装vue-router(vite中已经安装了，不需要重复安装)
```
yarn add vue-router
```

### 全局安装typescript(vite中已经安装了，不需要重复安装)
```
yarn global add typescript
生成tsconfig.json(typescript配置文件)
tsc --init
```

### 安装less
```
yarn add less less-loader

//引人全局样式,在main.ts 中引人
import "@/assets/styles/reset.less";
```

### 安装lodash
```
yarn add lodash
```

### 安装UUID nanoid
```
yarn add uuid
npm i --save-dev @types/uuid

yarn add nanoid
```

### 安装vue-flow
```
yarn add @vue-flow/core
在main.ts中引人样式
import '@vue-flow/core/dist/style.css';
import '@vue-flow/core/dist/theme-default.css';

yarn add @vue-flow/node-toolbar
```

### 安装阿里inconfont
```
1.下载iconfont js文件 放入到assets/js目录下
2.创建svgicon组件 在components/svgicon目录下
3.在main.ts中引人js和svgicon组件
import '@/assets/js/iconfont.js'
import SvgIcon from "@/components/svgicon/SvgIcon.vue";
4.使用
<svg class="icon" aria-hidden="true">
    <use xlink:href="图标名称"></use>
</svg>
```