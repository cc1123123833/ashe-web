/*
* 删除本地存储
* 主要是store中的持久化存储
* */
function removeStores(){
    //删除布局信息
    localStorage.removeItem("layoutStore");
    //删除用户信息
    localStorage.removeItem("useUserStore");
    //删除部门信息
    localStorage.removeItem("useDeptStore");
    //删除岗位信息
    localStorage.removeItem("useStationStore");
    //删除角色
    localStorage.removeItem("useRoleStore");
    //删除权限
    localStorage.removeItem("usePermissionStore");
    //删除菜单
    localStorage.removeItem("useMenuStore");
    //删除资源
    localStorage.removeItem("useResourceStore");
}

export {
    removeStores
}