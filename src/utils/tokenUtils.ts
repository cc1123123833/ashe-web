const TOKEN = "token"
//添加token
function setToken(token:any){
    localStorage.setItem(TOKEN,token);
}
//获取token
function getToken(){
    return localStorage.getItem(TOKEN);
}
//删除token
function removeToken(){
    localStorage.removeItem(TOKEN);
}

export {
    setToken,
    getToken,
    removeToken
}