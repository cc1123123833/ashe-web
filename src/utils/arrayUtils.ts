/*
* 递归查找数组中的对象
* list: 集合
* children: 子集合的属性名称
* attributeId: 对比的属性名称
* value: 对比属性值
* */
function findObjectToArray(list: Array<any>,
                           children: string,
                           attributeId: string,
                           value: string) {
    for (const item of list) {
        if (item[attributeId] === value) {
            return item;
        }
        if (item[children] && item[children].length > 0) {
            const sonitem:any = findObjectToArray(item[children], children, attributeId, value);
            if (sonitem) {
                return sonitem;
            }
        }
    }
    return null;
}

export {
    findObjectToArray
}