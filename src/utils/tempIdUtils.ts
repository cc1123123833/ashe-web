import { v4 as uuidv4 } from 'uuid';

const TEMPID = "TEMPID";
//获取web端临时id
function getTempId(){
    let tmpid = localStorage.getItem(TEMPID);
    if(!tmpid){
        tmpid = uuidv4().replaceAll("-","").toUpperCase();
        localStorage.setItem(TEMPID,tmpid);
    }
    return tmpid
}
//删除临时id
function removeTempId(){
    localStorage.removeItem(TEMPID);
}

export {
    getTempId,
    removeTempId
}