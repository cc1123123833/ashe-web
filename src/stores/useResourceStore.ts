import api from "@/api";
import {SUCCESS_CODE} from "@/common/base/baseStatus";
import {ElMessage} from "element-plus";

//引入类型
import type {
    ResourceOperationType,
    ResourceDeletVoType,
    ResourceTransferType
} from "@/common/types/resource/ResourceType";
import type {ResuleType} from "@/common/base/baseType";
import {defineStore} from "pinia";
import {ref} from "vue";

/**
 * 定义角色信息的state
 */
const state = () => {
    /**
     * 初始化用户对象响应式 state
     * ----------------------------------------------------
     */
    //资源的Transfer
    const resourceransferState = ref(new Array<ResourceTransferType>);
    /**
     * 定义getters
     * ----------------------------------------------------
     */
    const getResourceTransferState = () => {
        return resourceransferState;
    };
    /**
     * 定义actions
     * ----------------------------------------------------
     */
        //资源角色
    const addResourceActions = async (vo: ResourceOperationType) => {
            try {
                const re: ResuleType = await api.addResource(vo)
                //判断是否获取成功
                if (re.code === SUCCESS_CODE) {
                    //刷新Transfer
                    await getResourceTransferActions();
                    ElMessage.success(re.msg);
                } else {
                    throw new Error(re.msg);
                }
            } catch (error: any) {
                ElMessage.error(error.message);
            }
        };

    //修改资源
    const updateResourceActions = async (vo: ResourceOperationType) => {
        try {
            const re: ResuleType = await api.updateResource(vo)
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                //刷新Transfer
                await getResourceTransferActions();
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //通过ID删除资源(逻辑删除)
    const deleteResourceByIdActions = async (resourceId: string) => {
        try {
            const re: ResuleType = await api.deleteResourceById(resourceId)
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                //刷新Transfer
                await getResourceTransferActions();
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //批量删除资源(逻辑删除)
    const deleteResourceByIdsActions = async (vo: ResourceDeletVoType) => {
        try {
            const re: ResuleType = await api.deleteResourceByIds(vo);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                //刷新Transfer
                await getResourceTransferActions();
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //获取当前机构下的资源(Transfer)
    const getResourceTransferActions = async () => {
        try {
            const re: ResuleType = await api.getResourceTransfer();
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                resourceransferState.value = [...re.data];
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };

    //获取当前岗位下的角色(Transfer)
    const getResourceTransferByPermissionIdActions = async (permissionId:string) => {
        try {
            const re: ResuleType = await api.getResourceTransferByPermissionId(permissionId);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                return re.data;
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //暴露相关对象和方法
    return {
        resourceransferState,
        getResourceTransferState,
        addResourceActions,
        updateResourceActions,
        deleteResourceByIdActions,
        deleteResourceByIdsActions,
        getResourceTransferActions,
        getResourceTransferByPermissionIdActions
    }
};

const useResourceStore = defineStore('useResourceStore', state, {persist: true});


export default useResourceStore;