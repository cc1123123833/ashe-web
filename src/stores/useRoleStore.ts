import {defineStore} from 'pinia';
import api from "@/api";
import {SUCCESS_CODE} from "@/common/base/baseStatus";
import {ElMessage} from "element-plus";
import {ref} from "vue";

//引入类型
import type {ResuleType} from "@/common/base/baseType";
import type {
    RoleDeleteVoType,
    RoleOperationType,
    RoleTransferType
} from "@/common/types/role/RoleType";


/**
 * 定义角色信息的state
 */
const state = () => {
    /**
     * 初始化用户对象响应式 state
     * ----------------------------------------------------
     */
    //角色的Transfer
    const roleTransferState = ref(new Array<RoleTransferType>);
    /**
     * 定义getters
     * ----------------------------------------------------
     */
    const getRoleTransferState = () => {
        return roleTransferState;
    };
    /**
     * 定义actions
     * ----------------------------------------------------
     */
    //新增角色
    const addRoleActions = async (vo:RoleOperationType) => {
            try {
                const re: ResuleType = await api.addRole(vo)
                //判断是否获取成功
                if (re.code === SUCCESS_CODE) {
                    //更新角色的Transfer
                    await getRolesTransferActions();
                    ElMessage.success(re.msg);
                } else {
                    throw new Error(re.msg);
                }
            } catch (error: any) {
                ElMessage.error(error.message);
            }
    };
    //修改角色
    const updateRoleActions = async (vo:RoleOperationType) => {
        try {
            const re: ResuleType = await api.updateRole(vo)
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                //更新角色的Transfer
                await getRolesTransferActions();
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //通过ID删除角色(逻辑删除)
    const deleteRoleByIdActions = async (roleId: string) => {
        try {
            const re: ResuleType = await api.deleteRoleById(roleId)
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                //更新角色的Transfer
                await getRolesTransferActions();
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //批量删除角色(逻辑删除)
    const deleteRoleByIdsActions = async (vo: RoleDeleteVoType) => {
        try {
            const re: ResuleType = await api.deleteRoleByIds(vo);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                //更新角色的Transfer
                await getRolesTransferActions();
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };

    //获取当前机构下的角色(Transfer)
    const getRolesTransferActions = async () => {
        try {
            const re: ResuleType = await api.getRolesTransfer();
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                roleTransferState.value = [...re.data];
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };

    //获取当前岗位下的角色(Transfer)
    const getRolesTransferByStationIdActions = async (stationId:string) => {
        try {
            const re: ResuleType = await api.getRolesTransferByStationId(stationId);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                return re.data;
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };

    //暴露相关对象和方法
    return {
        roleTransferState,
        getRoleTransferState,
        addRoleActions,
        updateRoleActions,
        deleteRoleByIdActions,
        deleteRoleByIdsActions,
        getRolesTransferActions,
        getRolesTransferByStationIdActions
    }
}

const useRoleStore = defineStore('useRoleStore', state, {persist: true});


export default useRoleStore;