import {reactive, ref} from "vue";
import api from "@/api";
import {defineStore} from "pinia";
import type {ResuleType} from "@/common/base/baseType";
import {SUCCESS_CODE} from "@/common/base/baseStatus";
import {ElMessage} from "element-plus";

//引入类型
import type {
    MenuResourceDeleteVo,
    MenuResourceQueryVo,
    MenuTree,
    MenuWebVo,
    MenuResourceBindVo, RoleMenuVo
} from "@/common/types/menu/menuType";

const state = () => {
    /**
     * 初始化菜单对象响应式 state
     * menuTreeAll 菜单树用于菜单管理界面
     * menuChecked 选中的菜单
     * ----------------------------------------------------
     */
    //菜单树用于菜单管理界面
    const menuTreeAll = ref<Array<MenuTree>>(new Array<MenuTree>());
    //选中的菜单
    const menuChecked: MenuWebVo = reactive({
        menuId: '',
        menuCode: '',
        menuName: '',
        url: '',
        sort: 0,
        menuIcon: ''
    });
    /**
     * 定义getters
     * ----------------------------------------------------
     */
    //提供外部获取menuTreeAll的方法
    const getMenuTreeAll = () => {
        return menuTreeAll;
    };
    //提供外部获取menuChecked的方法
    const getMenuChecked = () => {
        return menuChecked;
    };
    /**
     * 定义actions
     * ----------------------------------------------------
     */
    //获取菜单信息
    const getMenuTreeAllActions = async () => {
        try {
            const re: ResuleType = await api.getMenuTreeByOrgId();
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                menuTreeAll.value= re.data;
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //快捷添加子菜单
    const addQuickMenu = async (parentId: string) => {
        try {
            const re: ResuleType = await api.addQuickMenu(parentId);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                //刷新菜单
                await getMenuTreeAllActions();
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //快捷删除子菜单
    const deleteQuickMenu = async (menuId: string) => {
        try {
            const re: ResuleType = await api.deleteQuickMenu(menuId);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                //刷新菜单
                await getMenuTreeAllActions();
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //根据菜单id查询菜单信息
    const selectMenuById = async (menuId: string) => {
        try {
            const re: ResuleType = await api.selectMenuById(menuId);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                const {
                    menuId,
                    menuCode,
                    menuName,
                    url,
                    sort,
                    menuIcon
                }:MenuWebVo = {...re.data};
                menuChecked.menuId = menuId;
                menuChecked.menuCode = menuCode;
                menuChecked.menuName = menuName;
                menuChecked.url = url;
                menuChecked.sort = sort;
                menuChecked.menuIcon = menuIcon;
                return re.data;
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //修改菜单信息
    const updateMenu = async (vo: MenuWebVo) => {
        try {
            const re: ResuleType = await api.updateMenu(vo);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                //刷新菜单
                await getMenuTreeAllActions();
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //根据菜单查询资源信息
    const selectMenuResourceByMenu = async (vo: MenuResourceQueryVo) => {
        try {
            const re: ResuleType = await api.selectMenuResourceByMenu(vo);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                return re.data;
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //批量删除菜单资源(物理删除)
    const deleteMenuResourceBatch = async (vo: MenuResourceDeleteVo) => {
        try {
            const re: ResuleType = await api.deleteMenuResourceBatch(vo);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //查询菜单现有的资源
    const selectMenuResource = async (menuId: string) => {
        try {
            const re: ResuleType = await api.selectMenuResource(menuId);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                return re.data;
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //批量添加菜单资源
    const addMenuResourceBatch = async (vo: MenuResourceBindVo) => {
        try {
            const re: ResuleType = await api.addMenuResourceBatch(vo);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };

    /*
    * 获取角色下的菜单
    * */
    const getMenuTreeByRoleId = async (roleId: string) => {
        try {
            const re: ResuleType = await api.getMenuByRoleId(roleId);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                return re.data;
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    }

    /*
    * 删除角色下的关联菜单
    */
    const deleteMenuByRole = async (vo: RoleMenuVo) => {
        try {
            const re: ResuleType = await api.deleteMenuByRole(vo);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    }
    /*
    * 添加角色下的关联菜单
    */
    const addMenuByRole = async (vo: RoleMenuVo) => {
        try {
            const re: ResuleType = await api.addMenuByRole(vo);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    }
    /*
    * 变更角色下的关联菜单
    */
    const updateMenuByRole = async (vo: RoleMenuVo) => {
        try {
            const re: ResuleType = await api.updateMenuByRole(vo);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    }
    //返回
    return {
        menuTreeAll,
        menuChecked,
        getMenuTreeAll,
        getMenuTreeAllActions,
        getMenuChecked,
        addQuickMenu,
        deleteQuickMenu,
        selectMenuById,
        updateMenu,
        selectMenuResourceByMenu,
        deleteMenuResourceBatch,
        selectMenuResource,
        addMenuResourceBatch,
        getMenuTreeByRoleId,
        deleteMenuByRole,
        addMenuByRole,
        updateMenuByRole
    };
}
const useMenuStore = defineStore('useMenuStore', state, {persist: true});

export default useMenuStore;