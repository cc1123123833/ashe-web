import {reactive} from "vue";
import {defineStore} from 'pinia';
//引入类型
import type {LayOutType, TabType} from "@/common/types/layout/LayOutType.ts";

const state = () => {
    /**
     * 初始化布局响应式 state
     * asideWidth 侧边栏宽度
     * tabList 选项卡列表
     * chooseTab 当前选中的选项卡
     * ----------------------------------------------------
     */
    const layoutState: LayOutType = reactive({
        asideWidth: '64px',
        tabList: new Array<TabType>(),
        chooseTab: ''
    });

    /**
     * 定义actions
     * ----------------------------------------------------
     */
    //修改侧边栏宽度
    const setAsideWidthActions = (w: number) => {
        layoutState.asideWidth = (w + "vw");
    };
    //添加选项卡
    const addTabActions = (tab: TabType) => {
        const have = layoutState.tabList.find((item) => item.name === tab.name);
        if(!have){
            //不存在则添加
            layoutState.tabList.push(tab);
        }
        //设置当前选中的选项卡
        layoutState.chooseTab = tab.name;
    };
    //删除选项卡
    const removeTabActions = (tab: TabType) => {
        //如果只有一个选项卡，则不允许删除
        if(layoutState.tabList && layoutState.tabList.length===1){
            return;
        }
        /*
        * 遍历获取当前被删除的选项卡的索引
        * 如果当前选项卡是最后一个，则默认选择前一个选项卡
        * 如果当前选项卡不是最后一个，则默认选择后一个选项卡
        * */
        let tmpTab: TabType = {} as TabType;
        layoutState.tabList.some((item:TabType,index:number) => {
            if(item.name === tab.name){
                tmpTab = layoutState.tabList[index + 1] || layoutState.tabList[index - 1]
                if (tmpTab) {
                    layoutState.chooseTab = tmpTab.name;
                    return true;
                }
            }
        });
        //过滤出被删除的选项卡
        const relist = layoutState.tabList.filter((item) => item.name !== tab.name);
        //重新赋值
        layoutState.tabList = [...relist]
    };
    //当前选中的选项卡
    const chooesTabActions = (name: string) => {
        layoutState.chooseTab = name;
    };
    //清空选项卡
    const clearTabActions = () => {
        layoutState.tabList = [];
        layoutState.chooseTab = '';
    };

    return {
        layoutState,
        setAsideWidthActions,
        addTabActions,
        removeTabActions,
        chooesTabActions,
        clearTabActions
    };
};

const useLayoutStore= defineStore("layoutStore", state,{persist: true});
export default useLayoutStore;
