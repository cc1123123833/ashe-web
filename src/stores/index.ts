//引人布局的store
import useLayoutStore from './useLayoutStore';
//引人用户信息的store
import useUserStore from './useUserStore';

//引入部门的store
import useDeptStore from './useDeptStore';

//映入岗位
import useStationStore from './useStationStore';

//引入角色的store
import useRoleStore from './useRoleStore';

//引人权限的store
import usePermissionStore from './usePermissionStore';

//引入资源
import useResourceStore from './useResourceStore';

//引入菜单
import useMenuStore from './useMenuStore';

//引入流水线
import usePipelineStore from './usePipelineStore';

/**
 * 统一暴露所有的store
 */
export {
    useUserStore,
    useLayoutStore,
    useDeptStore,
    useStationStore,
    useRoleStore,
    usePermissionStore,
    useResourceStore,
    useMenuStore,
    usePipelineStore
}