import {defineStore} from 'pinia';
import {ref} from "vue";
import api from "@/api/index";
import type {ResuleType} from "@/common/base/baseType";
import {SUCCESS_CODE} from "@/common/base/baseStatus";
import {ElMessage} from "element-plus";
/**
 * 引入类型
 */
import type {
    DeleteDeptType,
    DeptOperationVoType,
    DeptTreeType,
    DeptWebVo
} from "@/common/types/dept/DeptType";



const state = () => {
    /**
     * 初始化部门对象响应式 state
     * deptTree 部门树用于下拉和菜单
     * deptTreeAll 专门用于部门管理的TreeData
     * ----------------------------------------------------
     */
    const deptTree = ref<DeptTreeType>();

    const deptTreeAll = ref<Array<DeptWebVo>>(new Array<DeptWebVo>());
    /**
     * 定义getters
     * ----------------------------------------------------
     */
    //提供外部获取deptTree的方法
    const getDeptTree = () => {
            return deptTree;
    };
    //提供外部获取deptTreeAll的方法
    const getDeptTreeAll = () => {
        return deptTreeAll;
    };
    /**
     * 定义actions
     * ----------------------------------------------------
     */
    //获取部门信息
    const getDeptTreeActions = async () => {
            try {
                const re: ResuleType = await api.getDeptTreeByLoginInfo();
                //判断是否获取成功
                if (re.code === SUCCESS_CODE) {
                    deptTree.value= {...re.data};
                } else {
                    throw new Error(re.msg);
                }
            } catch (error: any) {
                ElMessage.error(error.message);
            }
    };

    //获取部门信息(不包含机构节点)
    const getDeptTreeAllActions = async () => {
            try {
                const re: ResuleType = await api.getDeptTreeAll();
                //判断是否获取成功
                if (re.code === SUCCESS_CODE) {
                    deptTreeAll.value= [...re.data];
                } else {
                    throw new Error(re.msg);
                }
            } catch (error: any) {
                ElMessage.error(error.message);
            }
    };

    //新增部门
    const addDeptActions= async (dept:DeptOperationVoType) => {
        try {
            const re: ResuleType = await api.addDept(dept);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                //更新部门数据
                await getDeptTreeAllActions();
                await getDeptTreeActions();
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };

    //修改部门
    const updateDeptActions= async (dept:DeptOperationVoType) => {
        try {
            const re: ResuleType = await api.updateDept(dept);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                //更新部门数据
                await getDeptTreeAllActions();
                await getDeptTreeActions();
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };

    //根据部门ID获取部门信息
    const getDeptByIdActions= async (deptId:string) => {
        try {
            const re: ResuleType = await api.getDeptById(deptId);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                return re.data;
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };

    //根据部门id删除部门信息并包含下级部门(逻辑删除)
    const deleteDeptByIdActions= async (deptId:string) => {
        try {
            const re: ResuleType = await api.deleteDeptById(deptId);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                //更新部门数据
                await getDeptTreeAllActions();
                await getDeptTreeActions();
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //批量删除部门信息并包含下级部门(逻辑删除)
    const deleteDeptByIdsActions= async (dept:DeleteDeptType) => {
        try {
            const re: ResuleType = await api.deleteDeptByIds(dept);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                //更新部门数据
                await getDeptTreeAllActions();
                await getDeptTreeActions();
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    }
    return {
        deptTree,
        getDeptTree,
        getDeptTreeActions,
        deptTreeAll,
        getDeptTreeAll,
        getDeptTreeAllActions,
        addDeptActions,
        updateDeptActions,
        getDeptByIdActions,
        deleteDeptByIdActions,
        deleteDeptByIdsActions
    }
}

const useDeptStore = defineStore('useDeptStore', state, {persist: true});

export default useDeptStore;