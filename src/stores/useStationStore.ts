import {defineStore} from 'pinia';
import {ref} from "vue";
import api from "@/api/index";
import {SUCCESS_CODE} from "@/common/base/baseStatus";
import {ElMessage} from "element-plus";
//引人类型
import type {ResuleType} from "@/common/base/baseType";
import type {
    StationOperationVoType,
    DeleteStationType,
    StationStateType,
    StationTransferType,
    StationUserVoType,
    StationRoleVoType,
    StationPermissionVoType
} from "@/common/types/station/StationType";

//定义岗位信息State
const state = () => {
    /**
     * 定义state
     * ----------------------------------------------------
     */
    /*
    * 定义岗位信息响应式 state
    * */
    const stationState = ref(new Array<StationStateType>);

    /**
     * 岗位的Transfer
     */
    const stationTransferState = ref(new Array<StationTransferType>);

    /**
     * 定义getters
     * ----------------------------------------------------
     */
        //提供外部获取stationState的方法
    const getStationState = () => {
            return stationState;
        };

    const getStationTransferState = () => {
        return stationTransferState;
    };
    /**
     * 定义actions
     * ----------------------------------------------------
     */
    //获取岗位信息
    const getStationStateActions = async () => {
            try {
                const re: ResuleType = await api.getStations();
                //判断是否获取成功
                if (re.code === SUCCESS_CODE) {
                    stationState.value = [...re.data];
                } else {
                    throw new Error(re.msg);
                }
            } catch (error: any) {
                ElMessage.error(error.message);
            }
    };
    //获取岗位Transfer信息
    const getStationStateTransferActions = async () => {
        try {
            const re: ResuleType = await api.getStationsTransfer();
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                stationTransferState.value = [...re.data];
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //获取当传入的用户id所在的机构下的岗位(Transfer)
    const getStationsTransferByUserIdActions = async (userId:string) => {
        try {
            const re: ResuleType = await api.getStationsTransferByUserId(userId);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                return re.data;
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };

    //新增岗位信息
    const addStationActions = async (station:StationOperationVoType) => {
        try {
            const re: ResuleType = await api.addStation(station);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                //更新岗位信息
                await getStationStateActions();
                await getStationStateTransferActions();
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //修改岗位
    const updateStationActions = async (station:StationOperationVoType) => {
        try {
            const re: ResuleType = await api.updateStation(station);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                //更新岗位信息
                await getStationStateActions();
                await getStationStateTransferActions();
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //批量删除岗位
    const deleteStationBatchActions = async (deleteVo: DeleteStationType) => {
        try{
            const re: ResuleType = await api.deleteStationBatch(deleteVo);
            if (re.code != SUCCESS_CODE) {
                //更新岗位信息
                await getStationStateActions();
                await getStationStateTransferActions();
                throw new Error(re.msg);
            }else{
                ElMessage.success(re.msg);
            }
        }catch (error: any) {
            ElMessage.error(error.message);
        }
    }

    /*
    * 删除岗位
    * */
    const deleteStationByIdActions = async (stationId: string) => {
        try{
            const re: ResuleType = await api.deleteStationById(stationId);
            if (re.code != SUCCESS_CODE) {
                throw new Error(re.msg);
            }else{
                ElMessage.success(re.msg);
            }
        }catch (error: any) {
            ElMessage.error(error.message);
        }
    }

    /*
    * 删除用户ID下的关联岗位
    * */
    const deleteStationsByUserIdActions = async (vo: StationUserVoType) => {
        try{
            const re: ResuleType = await api.deleteStationsByUserId(vo);
            if (re.code != SUCCESS_CODE) {
                throw new Error(re.msg);
            }else{
                ElMessage.success(re.msg);
            }
        }catch (error: any) {
            ElMessage.error(error.message);
        }
    }
    /*
    *新增用户ID下的关联岗位
    * */
    const addStationsByUserIdActions = async (vo: StationUserVoType) => {
        try{
            const re: ResuleType = await api.addStationsByUserId(vo);
            if (re.code != SUCCESS_CODE) {
                throw new Error(re.msg);
            }else{
                ElMessage.success(re.msg);
            }
        }catch (error: any) {
            ElMessage.error(error.message);
        }
    }
    /*
    * 删除岗位下的关联角色
    * */
    const deleteRolesByStationIdActions = async (vo: StationRoleVoType) => {
        try{
            const re: ResuleType = await api.deleteRolesByStationId(vo);
            if (re.code != SUCCESS_CODE) {
                throw new Error(re.msg);
            }else{
                ElMessage.success(re.msg);
            }
        }catch (error: any) {
            ElMessage.error(error.message);
        }
    }
    /*
    * 添加岗位下的关联角色
    * */
    const addRolesByStationIdActions = async (vo: StationRoleVoType) => {
        try{
            const re: ResuleType = await api.addRolesByStationId(vo);
            if (re.code != SUCCESS_CODE) {
                throw new Error(re.msg);
            }else{
                ElMessage.success(re.msg);
            }
        }catch (error: any) {
            ElMessage.error(error.message);
        }
    }
    /*
   * 删除岗位下的关联权限
   * */
    const deletePermissionByStationIdActions = async (vo: StationPermissionVoType) => {
        try{
            const re: ResuleType = await api.deletePermissionByStationId(vo);
            if (re.code != SUCCESS_CODE) {
                throw new Error(re.msg);
            }else{
                ElMessage.success(re.msg);
            }
        }catch (error: any) {
            ElMessage.error(error.message);
        }
    }
    /*
    * 添加岗位下的关联角色
    * */
    const addPermissionByStationIdActions = async (vo: StationPermissionVoType) => {
        try{
            const re: ResuleType = await api.addPermissionByStationId(vo);
            if (re.code != SUCCESS_CODE) {
                throw new Error(re.msg);
            }else{
                ElMessage.success(re.msg);
            }
        }catch (error: any) {
            ElMessage.error(error.message);
        }
    }
    //暴露相关state getters actions
    return {
        stationState,
        stationTransferState,
        getStationState,
        getStationTransferState,
        getStationStateActions,
        getStationStateTransferActions,
        getStationsTransferByUserIdActions,
        addStationActions,
        updateStationActions,
        deleteStationBatchActions,
        deleteStationByIdActions,
        deleteStationsByUserIdActions,
        addStationsByUserIdActions,
        deleteRolesByStationIdActions,
        addRolesByStationIdActions,
        deletePermissionByStationIdActions,
        addPermissionByStationIdActions
    }
};

const useStationStore = defineStore('useStationStore', state, {persist: true});

export default useStationStore;