import {defineStore} from 'pinia';
import {ref} from "vue";
import api from "@/api";
import {SUCCESS_CODE} from "@/common/base/baseStatus";
import {ElMessage} from "element-plus";

//引入类型
import type {
    PermissionDeleteVoType,
    PermissionOperationVoType, PermissionResourceVoType,
    PermissionTransferType
} from "@/common/types/permission/PermissionType";
import type {ResuleType} from "@/common/base/baseType";

/**
 * 定义权限信息的state
 */
const state = () => {
    /**
     * 初始化权限对象响应式 state
     * ----------------------------------------------------
     */
        //角色的Transfer
    const permissionTransferState = ref(new Array<PermissionTransferType>);

    /**
     * 定义getters
     * ----------------------------------------------------
     */

    /**
     * 定义actions
     * ----------------------------------------------------
     */
    //新增权限
    const addPermissionActions = async (vo: PermissionOperationVoType) => {
        try {
            const re: ResuleType = await api.addPermission(vo);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //修改权限
    const updatePermissionActions = async (vo: PermissionOperationVoType) => {
        try {
            const re: ResuleType = await api.updatePermission(vo);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //根据Id删除权限(逻辑删除)
    const deletePermissionByIdActions = async (permissionId: string) => {
        try {
            const re: ResuleType = await api.deletePermissionById(permissionId);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //批量删除权限(逻辑删除)
    const deletePermissionBatchActions = async (vo: PermissionDeleteVoType) => {
        try {
            const re: ResuleType = await api.deletePermissionBatch(vo);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    }

    //获取当前机构下的权限(Transfer)
    const getPermissionTransferActions = async () => {
        try {
            const re: ResuleType = await api.getPermissionTransfer();
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                permissionTransferState.value = [...re.data];
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };

    //获取当前岗位下的权限(Transfer)
    const getPermissionTransferByStationIdActions = async (stationId:string) => {
        try {
            const re: ResuleType = await api.getPermissionTransferByStationId(stationId);
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                return re.data;
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    /*
     * 删除权限下的关联资源
     * */
    const deleteResourceByPermissionIdActions = async (vo: PermissionResourceVoType) => {
        try{
            const re: ResuleType = await api.deleteResourceByPermissionId(vo);
            if (re.code != SUCCESS_CODE) {
                throw new Error(re.msg);
            }else{
                ElMessage.success(re.msg);
            }
        }catch (error: any) {
            ElMessage.error(error.message);
        }
    }
    /*
    * 添加岗位下的关联角色
    * */
    const addResourceByPermissionIdActions = async (vo: PermissionResourceVoType) => {
        try{
            const re: ResuleType = await api.addResourceByPermissionId(vo);
            if (re.code != SUCCESS_CODE) {
                throw new Error(re.msg);
            }else{
                ElMessage.success(re.msg);
            }
        }catch (error: any) {
            ElMessage.error(error.message);
        }
    }
    //暴露相关对象和方法
    return {
        permissionTransferState,
        addPermissionActions,
        updatePermissionActions,
        deletePermissionByIdActions,
        deletePermissionBatchActions,
        getPermissionTransferActions,
        deleteResourceByPermissionIdActions,
        addResourceByPermissionIdActions
    }
}

const usePermissionStore = defineStore('usePermissionStore', state, {persist: true});


export default usePermissionStore;