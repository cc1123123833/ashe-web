import {defineStore} from 'pinia';
import {reactive, ref} from "vue";
import api from "@/api/index";
//引入流水线类型
import type {
    PipelineQueryVoType,
    PipelineDeployVoType,
    PipelineDrawVoType
} from "@/common/types/pipeline/PipelineType";
//引入绘制元素类型
import type {Elements} from "@vue-flow/core";
import {ElMessage} from "element-plus";

const state = () => {
    /**
     * 初始化流水线对象响应式 state
     * ----------------------------------------------------
     */
    //当前绘制流水线中绘制的节点
    const drawPipelineChooseNodeId = ref<string>("");
    //当前绘制流水线绘制的对象
    const pipelineDrawObj = reactive<PipelineDrawVoType>({
        pipelineId: "",
        pipelineCode: "",
        pipelineName: "",
        pipelineDesc: "",
        pipelineJson: "",
    });
    //流水线绘制元素
    const elements = ref<Elements>(JSON.parse(pipelineDrawObj.pipelineJson || '[]'));

    /**
     * 定义getters
     * ----------------------------------------------------
     */
    //获取绘制流水线绘制的对象
    const getPipelineDrawObj = () => {
        return pipelineDrawObj;
    };
    //获取绘制元素数组
    const getElements = ()=>{
        return  elements;
    }
    /**
     * 定义actions
     * ----------------------------------------------------
     */
    //分页查询流水线信息
    const selectPipelinePageActions = async (query: PipelineQueryVoType) => {
        try {
            const re: any = await api.selectPipelinePage(query);
            //判断是否获取成功
            if (re.code === 200) {
                return re.data;
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };
    //空模板创建流水线
    const createEmptyPipelineActions = async () => {
        try {
            const re: any = await api.createEmptyPipeline();
            //判断是否获取成功
            if (re.code === 200) {
                /**
                 * 并赋值给绘制流水线对象
                 */
                //流水线ID
                pipelineDrawObj.pipelineId = re.data.pipelineId;
                //流水线编码
                pipelineDrawObj.pipelineCode = re.data.pipelineCode;
                //流水线名称
                pipelineDrawObj.pipelineName = re.data.pipelineName;
                return re.data;
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };

    //部署流水线
    const deployPipelineActions = async (vo:PipelineDeployVoType) => {
        try {
            const re: any = await api.deployPipeline(vo);
            //判断是否获取成功
            if (re.code === 200) {
                return re.data;
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };

    /*
    * 根据流水线ID查询流水线
    * */
    const selectPipelineByIdActions = async (pipelineId: string) => {
        try {
            const re: any = await api.selectPipelineById(pipelineId);
            //判断是否获取成功
            if (re.code === 200) {
                //流水线ID
                pipelineDrawObj.pipelineId = re.data.pipelineId;
                //流水线编码
                pipelineDrawObj.pipelineCode = re.data.pipelineCode;
                //流水线名称
                pipelineDrawObj.pipelineName = re.data.pipelineName;
                //流水线json
                pipelineDrawObj.pipelineJson = re.data.pipelineJson;
                //流水线节点
                elements.value = JSON.parse(pipelineDrawObj.pipelineJson || '[]');
                return re.data;
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };

    /*
    *根据流水线ID删除流水线
    * */
    const deletePipelineByIdActions = async (pipelineId: string) => {
        try {
            const re: any = await api.deletePipelineById(pipelineId);
            //判断是否获取成功
            if (re.code === 200) {
                ElMessage.success(re.msg);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };

    //重置流水线绘制对象
    const resetDrawObjActions = () => {
        //清理绘制流水线中绘制的节点
        drawPipelineChooseNodeId.value = "";
        //清理流水线绘制元素
        pipelineDrawObj.pipelineId = "";
        pipelineDrawObj.pipelineCode = "";
        pipelineDrawObj.pipelineName = "";
        pipelineDrawObj.pipelineDesc = "";
        pipelineDrawObj.pipelineJson = "";

        //清理绘制元素数组
        elements.value = [];
    };

    return {
        drawPipelineChooseNodeId,
        pipelineDrawObj,
        elements,
        getPipelineDrawObj,
        getElements,
        selectPipelinePageActions,
        createEmptyPipelineActions,
        deployPipelineActions,
        resetDrawObjActions,
        selectPipelineByIdActions,
        deletePipelineByIdActions
    }
}

const usePipelineStore = defineStore('usePipelineStore', state, {persist: true});

export default usePipelineStore;