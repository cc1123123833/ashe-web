import {defineStore} from 'pinia';
import {reactive, ref} from "vue";
import api from "@/api/index";
import {getTempId, removeTempId} from "@/utils/tempIdUtils";
import {removeToken, setToken} from "@/utils/tokenUtils";
import {removeStores} from "@/utils/localstoreUtils";
import {SUCCESS_CODE} from "@/common/base/baseStatus";
import {ElMessage} from "element-plus";

import type {ResuleType} from "@/common/base/baseType";
import type {
    AccountType,
    DeleteAccountType,
    LoginType,
    ResetPasswordType,
    UserInfoType
} from "@/common/types/user/UserType";


/**
 * 定义用户信息的state
 */
const state = () => {
    /**
     * 初始化用户对象响应式 state
     * ----------------------------------------------------
     */
        //用户信息
    let userInfoState: UserInfoType = reactive({});
    //用户角色
    const userRoles = ref(new Array<string>());
    //用户菜单
    const userMenus = ref(new Array<string>);
    /**
     * 定义getters
     * ----------------------------------------------------
     */
        //提供外部获取userInfo的方法,并可以加工处理
    const getUserInfo = () => {
            return userInfoState;
        };
    /**
     * 定义actions
     * ----------------------------------------------------
     *
     */
    /**
     * 登录认证后调用该方法
     * 获取用户信息,修改响应用户信息对象
     */
    const userInfoActions = async () => {
        try {
            const re: ResuleType = await api.getAccountInfo();
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                const {
                    accountId,
                    accountName,
                    userName,
                    orgId,
                    deptId,
                    orgName,
                    deptName
                } = re.data;
                //修改人员信息响应式对象
                userInfoState.accountId = accountId;
                userInfoState.accountName = accountName;
                userInfoState.userName = userName;
                userInfoState.orgId = orgId;
                userInfoState.deptId = deptId;
                userInfoState.orgName = orgName;
                userInfoState.deptName = deptName;
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };

    /*
    * 获取用户角色
    * */
    const userRolesActions = async () => {
        try {
            const re: ResuleType = await api.getRoles();
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                //修改响应角色对象
                userRoles.value = [...re.data];
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };

    /*
    * 获取用户菜单
    * */
    const userMenusActions = async () => {
        try {
            const re: ResuleType = await api.getUserMenusList();
            //判断是否获取成功
            if (re.code === SUCCESS_CODE) {
                //修改响应菜单对象
                userMenus.value = [...re.data];
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }
    };

    /**
     * 用户登录
     * @param loginfo
     */
    const loginActions = async (loginfo: LoginType) => {
        //获取临时ID
        loginfo.tempId = getTempId();
        //删除临时id
        removeTempId();
        //清空token
        removeToken();
        try {
            //调用登录接口
            const re: ResuleType = await api.login(loginfo);
            if (re.code === SUCCESS_CODE) {
                //记录token
                setToken(re.data.token);
            } else {
                throw new Error(re.msg);
            }
        } catch (error: any) {
            ElMessage.error(error.message);
        }

    }

    /**
     * 用户退出
     */
    const logoutActions = ()=>{
        //删除临时id
        removeTempId();
        //清空token
        removeToken();
        //清空localStorage
        removeStores();
    }

    /*
    * 新增账号
    * */
    const addAccountActions = async (account: AccountType) => {
        try{
            const re: ResuleType = await api.addAccount(account);
            if (re.code != SUCCESS_CODE) {
                throw new Error(re.msg);
            }else{
                ElMessage.success(re.msg);
            }
        }catch (error: any) {
            ElMessage.error(error.message);
        }

    }

    /*
    * 修改账号
    * */
    const updateAccountActions = async (account: AccountType) => {
        try{
            const re: ResuleType = await api.updateAccount(account);
            if (re.code != SUCCESS_CODE) {
                throw new Error(re.msg);
            }else{
                ElMessage.success(re.msg);
            }
        }catch (error: any) {
            ElMessage.error(error.message);
        }

    }

    /*
    * 删除账号
    * */
    const deleteAccountActions = async (accountId: string) => {
        try{
            const re: ResuleType = await api.deleteAccount(accountId);
            if (re.code != SUCCESS_CODE) {
                throw new Error(re.msg);
            }else{
                ElMessage.success(re.msg);
            }
        }catch (error: any) {
            ElMessage.error(error.message);
        }
    }

    /*
    * 批量删除账号
    * */
    const deleteAccountBatchActions = async (deleteVo: DeleteAccountType) => {
        try{
            const re: ResuleType = await api.deleteBatchAccount(deleteVo);
            if (re.code != SUCCESS_CODE) {
                throw new Error(re.msg);
            }else{
                ElMessage.success(re.msg);
            }
        }catch (error: any) {
            ElMessage.error(error.message);
        }
    }

    /*
   * 重置密码
   * */
    const resetPasswordActions = async (reset: ResetPasswordType) => {
        try{
            const re: ResuleType = await api.resetPassword(reset);
            if (re.code != SUCCESS_CODE) {
                throw new Error(re.msg);
            }else{
                ElMessage.success(re.msg);
            }
        }catch (error: any) {
            ElMessage.error(error.message);
        }
    }

    /**
     * 重置用户信息state
     */
    function $reset() {
        userInfoState = reactive({});
    }

    //暴露相关对象和方法
    return {
        //用户的信息data
        userInfoState,
        //用户角色
        userRoles,
        //用户菜单
        userMenus,
        //提供用户的data getter方法
        getUserInfo,
        //获取用户的action方法
        userInfoActions,
        //登录用户的action方法
        loginActions,
        //用户角色action方法
        userRolesActions,
        //用户菜单action方法
        userMenusActions,
        //用户退出action方法
        logoutActions,
        //新增账号action方法
        addAccountActions,
        //修改账号action方法
        updateAccountActions,
        //删除账号action方法
        deleteAccountActions,
        //批量删除账号action方法
        deleteAccountBatchActions,
        //重置密码action方法
        resetPasswordActions,
        //重写重置方法
        $reset
    };
};
/**
 * 创建一个用户信息的store
 * 1.参数1：store的名字(唯一标识)
 * 2.参数2：store的 state 对象/函数
 * 3.参数3：store的 持久化配置
 * ----------------------------------------------------
 * 注意：这里的 useUserStore 对象不能直接使用 ES6 的解构赋值，否则会导致 store中的属性响应式失效
 * 例如：const { userInfo, getUserInfo, actionsUserInfo } = useUserStore() 这样是错误的
 * 正确的做法是：
 *  const { userInfo, getUserInfo } = storeToRefs(useUserStore()),这里使用的 storeToRefs 方法结构属性，和计算属性
 *  const { actionsUserInfo } = useUserStore() ,actions方法可以直接解构
 * 修改state
 * 方法1：直接修改state对象的属性值，适合一次修改一个属性
 * store实例对象.属性 = 值
 * store.userInfo.accountId = '123'
 *
 * 方法2：使用$patch(对象)方法，适合一次修改多个属性。并且只会修改到传入的属性，不会修改其他属性【推荐】
 * store实例对象.$patch(对象{属性1:值1,属性2:值2})
 * store.$patch({accountId: '123', accountName: '张三'})
 *
 * 方式3：使用$pach(函数)方法，支持修改单个和多个属性。并且只会修改到传入的属性，不会修改其他属性【强烈推荐】
 * store实例对象.$patch((state) => {state.属性1 = 值1})
 */
const useUserStore = defineStore('useUserStore', state, {persist: true});


export default useUserStore

