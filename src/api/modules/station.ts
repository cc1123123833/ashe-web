import axios from "@/api/ajax";
//引人类型
import type {
    DeleteStationType,
    StationOperationVoType,
    StationQueryType,
    StationRoleVoType,
    StationUserVoType,
    StationPermissionVoType
} from "@/common/types/station/StationType";

/*
* 获取当前机构下的所有岗位
* */
function getStations() {
    return axios.get(`/base/station/getStationsByDeptId`);
}

/**
 * 根据机构id获取岗位(Transfer)
 */
function getStationsTransfer() {
    return axios.get(`/base/station/getStationsTransfer`);
}

//获取当传入的用户id所在的机构下的岗位(Transfer)
function getStationsTransferByUserId(userId: string) {
    return axios.get(`/base/station/getStationsTransferByUserId/${userId}`);
}

/*
* 根据部门id获取岗位
*/
function getStationsByDeptId(deptId: string) {
    return axios.get(`/base/station/getStationsByDeptId/${deptId}`);
}

//分页与获取当前机构的岗位信息
function getStationWebVoByPage(query: StationQueryType) {
    return axios.post(`/base/station/getStationWebVoByPage`, query);
}

//新增岗位
function addStation(station: StationOperationVoType) {
    return axios.post(`/base/station/addStation`, station);
}

//修改岗位
function updateStation(station: StationOperationVoType) {
    return axios.post(`/base/station/updateStation`, station);
}

//根据岗位ID查询岗位信息
function getStationById(stationId: string) {
    return axios.get(`/base/station/getStationWebVoById/${stationId}`);
}

//批量删除岗位
function deleteStationBatch(deleteVo: DeleteStationType) {
    return axios.post(`/base/station/deleteBatchStation`, deleteVo);
}

//根据岗位ID删除岗位(逻辑删除)
function deleteStationById(stationId: string) {
    return axios.delete(`/base/station/deleteStationById/${stationId}`);
}

//删除用户ID下的关联岗位
function deleteStationsByUserId(vo: StationUserVoType) {
    return axios.post(`/base/station/deleteStationsByUserId`, vo);
}

//新增用户ID下的关联岗位
function addStationsByUserId(vo: StationUserVoType) {
    return axios.post(`/base/station/addStationsByUserId`, vo);
}

//删除岗位下的关联角色
function deleteRolesByStationId(vo: StationRoleVoType) {
    return axios.post(`/base/station/deleteRolesByStationId`, vo);
}

//添加岗位下的关联角色
function addRolesByStationId(vo: StationRoleVoType) {
    return axios.post(`/base/station/addRolesByStationId`, vo);
}

//删除岗位下的关联权限
function deletePermissionByStationId(vo: StationPermissionVoType) {
    return axios.post(`/base/station/deletePermissionByStationId`, vo);
}

//添加岗位下的关联权限
function addPermissionByStationId(vo: StationPermissionVoType) {
    return axios.post(`/base/station/addPermissionByStationId`, vo);
}

export const stationHandler = {
    getStations,
    getStationsTransfer,
    getStationsTransferByUserId,
    getStationsByDeptId,
    getStationWebVoByPage,
    addStation,
    updateStation,
    getStationById,
    deleteStationBatch,
    deleteStationById,
    deleteStationsByUserId,
    addStationsByUserId,
    deleteRolesByStationId,
    addRolesByStationId,
    deletePermissionByStationId,
    addPermissionByStationId
}