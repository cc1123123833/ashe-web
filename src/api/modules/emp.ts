import axios from "@/api/ajax";
import type {
    AccountType,
    DeleteAccountType,
    ResetPasswordType,
    UserQueryType
} from "@/common/types/user/UserType";

//分页与获取当前机构的用户信息
function selectUserVoByQueryPage(query:UserQueryType){
    return axios.post(`/base/account/selectUserVoByQueryPage`,query);
}
//查询单个账号信息
function selectUserEditVoById(accountId:string){
    return axios.get(`/base/account/selectUserEditVoById/${accountId}`);
}

//新增账号
function addAccount(userOperationVo:AccountType){
    return axios.post(`/base/account/addAccount`,userOperationVo);
}

//修改账号
function updateAccount(userOperationVo:AccountType){
    return axios.put(`/base/account/updateAccount`,userOperationVo);
}

//逻辑删除账号
function deleteAccount(accountId:string){
    return axios.delete(`/base/account/deleteAccount/${accountId}`);
}

//批量逻辑删除账号
function deleteBatchAccount(deleteVo:DeleteAccountType){
    return axios.post(`/base/account/deleteBatchAccount`,deleteVo);
}

//重置密码
function resetPassword(reset:ResetPasswordType){
    return axios.post(`/base/account/resetPwd`,reset);
}

export const empHandler = {
    selectUserVoByQueryPage,
    selectUserEditVoById,
    addAccount,
    updateAccount,
    deleteAccount,
    deleteBatchAccount,
    resetPassword
}