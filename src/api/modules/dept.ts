import axios from "@/api/ajax";
//引入类型
import type {
    DeptOperationVoType,
    DeleteDeptType
} from "@/common/types/dept/DeptType";

/**
 * 获取当前登录人所在机构的部门
 * @param loginfo
 */
function getDeptTreeByLoginInfo() {
    return axios.get(`/base/dept/getDeptTreeByLoginInfo`);
}

/*
* 获取当前用户的所在的机构全量部门树(不包含机构节点)
* */
function getDeptTreeAll() {
    return axios.get(`/base/dept/getDeptTreeAll`);
}

/*
* 新增部门
* */
function addDept(dept:DeptOperationVoType){
    return axios.post(`/base/dept/addDept`,dept);
}

/*
* 修改部门
* */
function updateDept(dept:DeptOperationVoType){
    return axios.post(`/base/dept/updateDept`,dept);
}

/*
* 根据部门ID获取部门信息
* */
function getDeptById(deptId:string){
    return axios.get(`/base/dept/selectDeptById/${deptId}`);
}

/*
* 根据部门id删除部门信息并包含下级部门(逻辑删除)
* */
function deleteDeptById(deptId:string){
    return axios.delete(`/base/dept/deleteDeptById/${deptId}`);
}

/*
 * 批量删除部门信息并包含下级部门(逻辑删除)
 */
function deleteDeptByIds(dept:DeleteDeptType){
    return axios.post(`/base/dept/deleteDeptByIds`,dept);
}

export const deptHandler = {
    getDeptTreeByLoginInfo,
    getDeptTreeAll,
    addDept,
    updateDept,
    getDeptById,
    deleteDeptById,
    deleteDeptByIds
}