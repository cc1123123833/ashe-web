import axios from "@/api/ajax";
import type {
    MenuResourceBindVo,
    MenuResourceDeleteVo,
    MenuResourceQueryVo,
    MenuWebVo, RoleMenuVo
} from "@/common/types/menu/menuType";

/**
 * 查询机构下所有的菜单(Tree)
 */
function getMenuTreeByOrgId() {
    return axios.get(`/base/menu/getMenuTreeByOrgId`);
}
/*
* 快捷添加子菜单
*/
function addQuickMenu(parentId:string) {
    return axios.get(`/base/menu/addQuickMenu/${parentId}`);
}

/*
* 快捷删除子菜单
*/
function deleteQuickMenu(menuId:string) {
    return axios.delete(`/base/menu/deleteQuickMenu/${menuId}`);
}

/*
* 根据菜单id查询菜单信息
*/
function selectMenuById(menuId:string) {
    return axios.get(`/base/menu/selectMenuById/${menuId}`);
}

/*
* 修改菜单信息
*/
function updateMenu(vo:MenuWebVo) {
    return axios.put(`/base/menu/updateMenu`,vo);
}

/*
* 根据菜单查询资源信息
*/
function selectMenuResourceByMenu(vo: MenuResourceQueryVo) {
    return axios.post(`/base/menuResource/selectMenuResourceByMenu`,vo);
}

/*
* 批量删除菜单资源(物理删除)
* */
function deleteMenuResourceBatch(vo: MenuResourceDeleteVo) {
    return axios.post(`/base/menuResource/deleteMenuResourceBatch`,vo);
}

/**
 * 查询菜单现有的资源
 */
function selectMenuResource(menuId:string) {
     return axios.get(`/base/menuResource/selectMenuResource/${menuId}`);
}

/**
 * 批量添加菜单资源
 */
function addMenuResourceBatch(vo: MenuResourceBindVo) {
    return axios.post(`/base/menuResource/addMenuResourceBatch`,vo);
}

/*
* 获取角色下的菜单
*/
function getMenuByRoleId(roleId:string) {
    return axios.get(`/base/menu/getMenuByRoleId/${roleId}`);
}

/*
* 删除角色下的关联菜单
*/
function deleteMenuByRole(vo: RoleMenuVo) {
    return axios.post(`/base/menu/deleteMenuByRole`,vo);
}
/*
* 添加角色下的关联菜单
*/
function addMenuByRole(vo: RoleMenuVo) {
    return axios.post(`/base/menu/addMenuByRole`,vo);
}
/*
* 变更角色下的关联菜单
*/
function updateMenuByRole(vo: RoleMenuVo) {
    return axios.post(`/base/menu/updateMenuByRole`,vo);
}
export const menuHandler = {
    getMenuTreeByOrgId,
    addQuickMenu,
    deleteQuickMenu,
    selectMenuById,
    updateMenu,
    selectMenuResourceByMenu,
    deleteMenuResourceBatch,
    selectMenuResource,
    addMenuResourceBatch,
    getMenuByRoleId,
    deleteMenuByRole,
    addMenuByRole,
    updateMenuByRole
}