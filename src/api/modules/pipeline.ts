import axios from "@/api/ajax";

//引入类型
import type {PipelineDeployVoType, PipelineQueryVoType} from "@/common/types/pipeline/PipelineType";

/*
* 分页查询流水线信息
* */
function selectPipelinePage(query: PipelineQueryVoType) {
    return axios.post(`/pipeline/pipeline/selectPipelinePage`, query);
}

/*
*空模板创建流水线
*/
function createEmptyPipeline() {
    return axios.get(`/pipeline/pipeline/createEmptyPipeline`);
}

/**
 * 部署流水线
 */
function deployPipeline(vo:PipelineDeployVoType) {
    return axios.post(`/pipeline/pipeline/deployPipeline`,vo);
}

/*
* 根据流水线ID查询流水线
* */
function selectPipelineById(pipelineId: string) {
    return axios.get(`/pipeline/pipeline/selectPipelineById/${pipelineId}`);
}

/*
* 根据流水线ID删除流水线
* */
function deletePipelineById(pipelineId: string) {
    return axios.delete(`/pipeline/pipeline/deletePipelineById/${pipelineId}`);
}

export const pipelineHandler = {
    selectPipelinePage,
    createEmptyPipeline,
    deployPipeline,
    selectPipelineById,
    deletePipelineById
}