import axios from "@/api/ajax";

//引入类型
import type {
    RoleDeleteVoType,
    RoleOperationType,
    RoleQueryType
} from "@/common/types/role/RoleType";

/*
* 查询机构下的角色
* */
function selectRoleByQuery(query: RoleQueryType) {
    return axios.post(`/base/role/selectRoleByQuery`,query);
}

/*
* 新增角色
* */
function addRole(vo: RoleOperationType) {
    return axios.post(`/base/role/addRole`,vo);
}

/*
* 根据角色ID查询角色信息
* */
function getRoleById(roleId: string) {
    return axios.get(`/base/role/getRoleById/${roleId}`);
}

/*
* 更新角色
* */
function updateRole(vo: RoleOperationType) {
    return axios.put(`/base/role/updateRole`,vo);
}

/*
* 通过ID删除角色(逻辑删除)
* */
function deleteRoleById(roleId: string) {
    return axios.delete(`/base/role/deleteRoleById/${roleId}`);
}

/*
* 批量删除角色(逻辑删除)
* */
function deleteRoleByIds(vo: RoleDeleteVoType) {
    return axios.post(`/base/role/deleteRoleByIds`,vo);
}

//获取当前机构下的角色(Transfer)
function getRolesTransfer() {
    return axios.get(`/base/role/getRolesTransfer`);
}
//获取当前岗位下的角色(Transfer)
function getRolesTransferByStationId(stationId:string) {
    return axios.get(`/base/role/getRolesTransferByStationId/${stationId}`);
}

export const roleHandler = {
    selectRoleByQuery,
    addRole,
    getRoleById,
    updateRole,
    deleteRoleById,
    deleteRoleByIds,
    getRolesTransfer,
    getRolesTransferByStationId
}