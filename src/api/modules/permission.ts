import axios from "@/api/ajax";

//引入类型
import type {
    PermissionDeleteVoType,
    PermissionOperationVoType,
    PermissionQuery, PermissionResourceVoType
} from "@/common/types/permission/PermissionType";

//查询机构下的权限
function selectPermissionByQuery(query: PermissionQuery) {
    return axios.post(`/base/permission/selectPermissionByQuery`,query);
}

//根据Id查询权限
function selectPermissionById(permissionId: string) {
    return axios.get(`/base/permission/selectPermissionById/${permissionId}`);
}

//新增权限
function addPermission(vo: PermissionOperationVoType) {
    return axios.post(`/base/permission/addPermission`,vo);
}
//修改权限
function updatePermission(vo: PermissionOperationVoType) {
    return axios.put(`/base/permission/updatePermission`,vo);
}

//根据Id删除权限(逻辑删除)
function deletePermissionById(permissionId: string) {
    return axios.delete(`/base/permission/deletePermissionById/${permissionId}`);
}
//批量删除权限(逻辑删除)
function deletePermissionBatch(vo: PermissionDeleteVoType) {
    return axios.post(`/base/permission/deletePermissionBatch`,vo);
}

//获取当前机构下的权限(Transfer)
function getPermissionTransfer() {
    return axios.get(`/base/permission/getPermissionTransfer`);
}
//获取当前岗位下的权限(Transfer)
function getPermissionTransferByStationId(stationId:string) {
    return axios.get(`/base/permission/getPermissionTransferByStationId/${stationId}`);
}

//删除权限下的关联资源
function deleteResourceByPermissionId(vo: PermissionResourceVoType) {
    return axios.post(`/base/permission/deleteResourceByPermissionId`, vo);
}

//添加权限下的关联资源
function addResourceByPermissionId(vo: PermissionResourceVoType) {
    return axios.post(`/base/permission/addResourceByPermissionId`, vo);
}
export const permissionHandler = {
    selectPermissionByQuery,
    selectPermissionById,
    addPermission,
    updatePermission,
    deletePermissionById,
    deletePermissionBatch,
    getPermissionTransfer,
    getPermissionTransferByStationId,
    deleteResourceByPermissionId,
    addResourceByPermissionId
}