import axios from "@/api/ajax";
//引入类型
import type { LoginType } from "@/common/types/user/UserType";

/**
 * 登录接口
 * @param loginfo
 */
function login(loginfo:LoginType) {
    return axios.post(`/ca/login`,loginfo,{headers:{'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8'}});
}

/**
 * 获取用户信息
 */
function getAccountInfo() {
    return axios.post(`/ca/account/getAccountInfo`);
}

/**
 * 获取用户角色
 */
function getRoles() {
    return axios.post(`/ca/account/getRoles`);
}

/**
 * 获取用户菜单(Tree)
 */
function getUserMenusTree() {
    return axios.post(`/ca/account/getUserMenusTree`);
}

/**
 * 获取用户菜单(List)
 */
function getUserMenusList() {
    return axios.post(`/ca/account/getUserMenusList`);
}

export const loginHandler = {
    login,
    getAccountInfo,
    getRoles,
    getUserMenusTree,
    getUserMenusList
}