import axios from "@/api/ajax";

//引入类型
import type {
    ResourceDeletVoType,
    ResourceOperationType,
    ResourceQueryType
} from "@/common/types/resource/ResourceType";

/*
* 查询机构下的资源
* */
function selectResourceByQuery(query: ResourceQueryType) {
    return axios.post(`/base/resource/selectResourceByQuery`,query);
}

/*
* 新增资源
* */
function addResource(vo: ResourceOperationType) {
    return axios.post(`/base/resource/addResource`,vo);
}

/*
* 根据ID获取资源信息
* */
function getResourceById(resourceId: string) {
    return axios.get(`/base/resource/getResourceById/${resourceId}`);
}

/*
* 修改资源
* */
function updateResource(vo: ResourceOperationType) {
    return axios.put(`/base/resource/updateResource`,vo);
}

/*
* 通过ID删除资源(逻辑删除)
* */
function deleteResourceById(resourceId: string) {
    return axios.delete(`/base/resource/deleteResourceById/${resourceId}`);
}

/*
* 批量删除角色(逻辑删除)
* */
function deleteResourceByIds(vo: ResourceDeletVoType) {
    return axios.post(`/base/resource/deleteResourceByIds`,vo);
}

//获取当前机构下的资源(Transfer)
function getResourceTransfer() {
    return axios.get(`/base/resource/getResourceTransfer`);
}
//获取当前权限下的资源(Transfer)
function getResourceTransferByPermissionId(permissionId:string) {
    return axios.get(`/base/resource/getResourceTransferByPermissionId/${permissionId}`);
}

export const resourceHander = {
    selectResourceByQuery,
    addResource,
    getResourceById,
    updateResource,
    deleteResourceById,
    deleteResourceByIds,
    getResourceTransfer,
    getResourceTransferByPermissionId
}