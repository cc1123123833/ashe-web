/**
 * 这里写入全部的异步请求
 */
//登录用户管理
import {loginHandler} from "@/api/modules/login";
//部门管理
import {deptHandler} from "@/api/modules/dept";
//职员管理
import {empHandler} from "@/api/modules/emp";
//岗位管理
import {stationHandler} from "@/api/modules/station";
//角色管理
import {roleHandler} from "@/api/modules/role";
//权限管理
import {permissionHandler} from "@/api/modules/permission";
//引入资源管理
import {resourceHander} from "@/api/modules/resource";
//引入菜单管理
import {menuHandler} from "@/api/modules/menu";
//引入流水线管理
import {pipelineHandler} from "@/api/modules/pipeline";

const api = {
    ...loginHandler,
    ...deptHandler,
    ...empHandler,
    ...stationHandler,
    ...roleHandler,
    ...permissionHandler,
    ...resourceHander,
    ...menuHandler,
    ...pipelineHandler
}
export default api;