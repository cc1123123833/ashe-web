/**
 * axios二次封装
 * 1.设置基础路径和超时间
 * 2.提取响应数据 data
 * 3.统一处理异常
 * 4.添加进度条
 */
import axios from "axios";
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import { ElMessage } from 'element-plus'
import {getToken} from "@/utils/tokenUtils";


/**
 * * 设置默认人配置项
 * * 设置请求头
 */
axios.defaults.headers.common['Content-Type'] = 'application/json';

/**
 * 创建基础请求函数
 * 1.设置基础路径(在vue.config配置了代理)
 * 2.并设置超时时间
 */
const service = axios.create({
    baseURL: "http://localhost:9527/",
    timeout: 6000
});

/**
 *  添加请求拦截器
 *  1.设置进度调开始
 *  2.设置token(后续添加)
 */
service.interceptors.request.use((config) => {
    NProgress.start();
    const token = getToken();
    if(token){
        //添加到请求头
        config.headers.token=token;
        //添加到请求体(防止请求头丢失)
        const bodyData = {
            token
        }
        config.data={
            ...config.data,
            ...bodyData
        }
    }
    return config;
});

/**
 * 添加响应拦截器
 * 1.提取Promise data
 * 2.统一处理异常
 * 3.结束进度条
 */
service.interceptors.response.use(
    response => {
        NProgress.done();
        return response.data;
    },
    error => {
        let msg = error.message || "你的请求走丢了";
        const status = error.response.status;
        if(status >= 400 && status < 500){
            msg="权限异常";
            ElMessage.error(msg);
        }
        NProgress.done();
        return Promise.reject({message:msg});
    })

export default service;