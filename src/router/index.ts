import { createRouter, createWebHistory} from 'vue-router';
import type {RouteRecordRaw} from 'vue-router';

//引入路由配置
import routers from '@/router/routers';
//引人token工具类
import {getToken} from "@/utils/tokenUtils";
//引入store
import {useLayoutStore} from "@/stores";

const routes: Array<RouteRecordRaw> = routers;

//创建路由对象
const router = createRouter({
    /**
     * 配置路由的历史模式
     * --------------------------------------------------------------
     * 哈希模式（Hash Mode）
     * URL 中的路由信息会以 # 符号后的部分来表示，
     * 例如：http://www.xxx.com/#/home，
     * #符号后的内容不会被包含在请求中，因此改变 # 后的内容不会重新加载页面。
     * ----------------------------------------------------------------
     * 历史模式（History Mode）
     * 将路由信息显示在 URL 的路径中，例如：http://www.xxx.com/home，
     * 但是这种模式下需要后端配置支持，否则会出现 404 错误。
     * 修改路由路径后，页面会重新加载。
     */
    history: createWebHistory(import.meta.env.BASE_URL),
    /**
     * 导入路由对象
     */
    routes
});

/**
 * 添加全局路由守卫
 */
router.beforeEach((to,from,next)=>{
    if (import.meta.env.VITE_OFFINE_DEV === 'true') {
        next()
    }

    /**
     * 获取路由 meta中的auth
     * 如果为true则需要认证后才能访问
     * false 不需要认证，可以直接访问
     */
    const auth = to.meta.auth;
    //获取token
    const token = getToken();

    /**
     * 不需要认证的逻辑
     * 1.判断是否是去登录页面
     *  1.1 如果是并存在token(说明已经登录过),直接跳转到首页
     *  1.2 如果不是,直接放行
     */
    if(!auth){
        if(token&&to.path==="/login"){
            //已经登录了，但是要去登录页，直接跳转到首页
            next("/home");
        }else{
            //未登录登录过,直接放行没有权限访问的路由
            next();
        }
    }

    /**
     * 需要认证的逻辑
     * 1.判断是否存在token
     *  1.1 存在则说明登录过
     *   1.1.1 如果是去登录页，直接跳转到首页
     *   1.1.2 如果不是，直接放行
     * 1.2 不存在token则说明没有登录过，跳转到登录页
     */
    if(auth){
        //判断是否登录
        if(token){
            if(to.path==="/login"){
                //已经登录了，但是要去登录页，直接跳转到首页
                next("/home");
            }else{
                //登录过,直接放行需要有权限放行的路由
                next();
            }
        }else{
            //没有登录,跳转到 登录界面
            next("/login");
        }
    }
});
/*
* 添加全局后置路由守卫
* 1.如果是需要及嵌入tab的路由，将路由信息添加到tabList中
* 2.如果不是，则清空tabList
* */
router.afterEach((to,from)=>{
   const meta = to.meta;
   const layoutStore = useLayoutStore();
   if(meta && meta.isTab){
       layoutStore.addTabActions({name: to.path, title: meta.title!});
   }else{
       layoutStore.clearTabActions();
   }
});
export default router;