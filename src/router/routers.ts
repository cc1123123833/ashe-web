import type {RouteRecordRaw} from 'vue-router';
import type {RouteMeta} from "vue-router";

//login路由
import loginRouters from '@/views/Login/LoginRouters';
//home路由
import homeRouters from '@/views/Home/HomeRouters';
/**
 * 统一配置动态路由组件
 */

/*
* 定义RouteMeta类型
* */
declare module 'vue-router' {
  interface RouteMeta {
    //是否是菜单
    menu?: boolean;
    //菜单名称
    title?: string;
    //菜单图标
    icon?: string;
    //是否有需要权限进入
    auth?: boolean;
    //菜单排序
    order?: number;
    //是否添加到Tab标签页
    isTab?: boolean;
  }
}

//路由配置
const routes: Array<RouteRecordRaw> = [
  loginRouters,
  homeRouters,
  {
    path: "/",
    redirect: "/login",
  }
];

export default routes;
