import {createApp} from 'vue'

import App from './App.vue'
import router from './router'

//引入全局样式
import "@/assets/styles/reset.less";
//引入流程图样式vue-flow
import '@vue-flow/core/dist/style.css';
import '@vue-flow/core/dist/theme-default.css';

//引人element-plus
import ElementPlus from 'element-plus';
//引人element-plus样式
import 'element-plus/dist/index.css';
// 引入element-plus 字体图标库
import * as ElementPlusIconsVue from '@element-plus/icons-vue';
// 引入国际化
import zhCn from 'element-plus/es/locale/lang/zh-cn';
//引入pinia
import {createPinia} from 'pinia'
//引入pinia持久化
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

//引人阿里inconfont
import '@/assets/js/iconfont.js'
import SvgIcon from "@/components/svgicon/SvgIcon.vue";

//创建app
const app = createApp(App);

// 注册element-plus 字体图标库
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
//注册svg-icon
app.component('svg-icon', SvgIcon);

//创建pinia
const pinia = createPinia();
//持久化pinia
pinia.use(piniaPluginPersistedstate);
//注册pinia
app.use(pinia);
//使用router
app.use(router);
//使用element-plus + 国际化
app.use(ElementPlus, {locale: zhCn});
app.mount('#app');
