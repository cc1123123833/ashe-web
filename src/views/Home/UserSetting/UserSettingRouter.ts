import type {RouteRecordRaw} from 'vue-router';

//用户设置
const UserSetting = () => import('@/views/Home/UserSetting/UserSetting.vue');

//用户管理模块
const UserManagement = () => import('@/views/Home/UserSetting/UserManagement/UserManagement.vue');
//部门管理模块
const DeptManagement = () => import('@/views/Home/UserSetting/DeptManagement/DeptManagement.vue');
//岗位管理模块
const StationManagement = () => import('@/views/Home/UserSetting/StationManagement/StationManagement.vue');
//角色
const RoleManagement = () => import('@/views/Home/UserSetting/RoleManagement/RoleManagement.vue');
//权限
const PermissionManagement = () => import('@/views/Home/UserSetting/PermissionManagement/PermissionManagement.vue');
//菜单
const MenuManagement = () => import('@/views/Home/UserSetting/MenuManagement/MenuManagement.vue');
//资源
const ResourceManagement = () => import('@/views/Home/UserSetting/ResourceManagement/ResourceManagement.vue');

const userSettingRouters: RouteRecordRaw = {
    path: "setting",
    name: "setting",
    component: UserSetting,
    meta: {
        menu: true,
        title: "设置",
        icon: "Setting",
        auth: false,
        order: 99,
        isTab: false,
        keepAlive: true
    },
    children: [
        {
            path: "usermanager",
            name: "usermanager",
            component: UserManagement,
            meta: {
                menu: true,
                title: "用户管理",
                icon: "User",
                auth: false,
                order: 1,
                isTab: true,
                keepAlive: true
            }
        },
        {
            path: "deptmanager",
            name: "deptmanager",
            component: DeptManagement,
            meta: {
                menu: true,
                title: "部门管理",
                icon: "School",
                auth: false,
                order: 2,
                isTab: true,
                keepAlive: true
            }
        },
        {
            path: "stationmanager",
            name: "stationmanager",
            component: StationManagement,
            meta: {
                menu: true,
                title: "岗位管理",
                icon: "Postcard",
                auth: false,
                order: 3,
                isTab: true,
                keepAlive: true
            }
        },
        {
            path: "rolemanager",
            name: "rolemanager",
            component: RoleManagement,
            meta: {
                menu: true,
                title: "角色管理",
                icon: "Avatar",
                auth: false,
                order: 4,
                isTab: true,
                keepAlive: true
            }
        },
        {
            path: "permissionmanager",
            name: "permissionmanager",
            component: PermissionManagement,
            meta: {
                menu: true,
                title: "权限管理",
                icon: "UserFilled",
                auth: false,
                order: 5,
                isTab: true,
                keepAlive: true
            }
        },
        {
            path: "menumanager",
            name: "menumanager",
            component: MenuManagement,
            meta: {
                menu: true,
                title: "菜单管理",
                icon: "Expand",
                auth: false,
                order: 6,
                isTab: true,
                keepAlive: true
            }
        },
        {
            path: "resourcemanager",
            name: "resourcemanager",
            component: ResourceManagement,
            meta: {
                menu: true,
                title: "资源管理",
                icon: "Fold",
                auth: false,
                order: 7,
                isTab: true,
                keepAlive: true
            }
        }
    ]
}

export default userSettingRouters;