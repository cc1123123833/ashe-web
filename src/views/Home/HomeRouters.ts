import type {RouteRecordRaw} from 'vue-router';
//引人工作台路由
import workRouters from '@/views/Home/Workbench/WorkRouters';
//引入工作流路由
import workflowRouters from '@/views/Home/Workflow/WorkflowRouters';
//引入用户管理路由
import userSettingRouters from '@/views/Home/UserSetting/UserSettingRouter';
//首页
const Home = () => import('@/views/Home/Home.vue');

//首页路由
const homeRouters:RouteRecordRaw = {
        path: "/home",
        name: "home",
        component: Home,
        redirect: 'home/workbench',
        meta: {
            menu: false,
            title: "首页",
            icon: "home",
            auth: true,
            order: 1,
            isTab: false,
            keepAlive: true
        },
        children: [
            workRouters,
            workflowRouters,
            userSettingRouters
        ]
}

export default homeRouters;