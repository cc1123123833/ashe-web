import type {RouteRecordRaw} from 'vue-router';

//工作台
const Workbench = () => import('@/views/Home/Workbench/Workbench.vue');
const workTable = () => import('@/views/Home/Workbench/workTable.vue');
//我的看板
const Board = () => import('@/views/Home/Workbench/Board/Board.vue');

const workRouters: RouteRecordRaw = {
    path: "workbench",
    name: "workbench",
    component: workTable,
    meta: {
        menu: true,
        title: "工作台",
        icon: "House",
        auth: true,
        order: 1,
        isTab: false,
        keepAlive: true
    },
    // children: [
    //     {
    //         path: "board",
    //         name: "board",
    //         component: Board,
    //         meta: {
    //             menu: true,
    //             title: "我的看板",
    //             icon: "PieChart",
    //             auth: false,
    //             order: 1,
    //             isTab: true,
    //             keepAlive: true
    //         }
    //     },
    // ]
}

export default workRouters;