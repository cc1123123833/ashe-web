import type {RouteRecordRaw} from 'vue-router';

//工作流
const Workflow = () => import('@/views/Home/Workflow/Workflow.vue');
//流程管理
const ProcessManagement = () => import('@/views/Home/Workflow/ProcessManagement/ProcessManagement.vue');

const workflowRouters: RouteRecordRaw = {
    path: "workflow",
    name: "workflow",
    component: Workflow,
    meta: {
        menu: true,
        title: "持续集成",
        icon: "Histogram",
        auth: true,
        order: 1,
        isTab: false,
        keepAlive: true
    },
    children: [
        {
            path: "processmanager",
            name: "processmanager",
            component: ProcessManagement,
            meta: {
                menu: true,
                title: "流水线",
                icon: "Share",
                auth: false,
                order: 1,
                isTab: true,
                keepAlive: true
            }
        },
    ]
}

export default workflowRouters;