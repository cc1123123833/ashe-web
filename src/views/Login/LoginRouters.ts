import type {RouteRecordRaw} from 'vue-router';

//登录页面
const Login = () => import('@/views/Login/Login.vue');

const loginRouters: RouteRecordRaw = {
    path: "/login",
    name: "login",
    component: Login,
    meta: {
        menu: false,
        title: "登录",
        icon: "login",
        auth: false,
        isTab: false
    }
}

export default loginRouters;