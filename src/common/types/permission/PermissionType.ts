/*
* 权限统一类型
* */
//引入分页类型
import type {PageType} from "@/common/base/baseType";

/*
* 权限表格类型
* */
type PermissionTableType={
    //权限ID
    permissionId: string,
    //所属机构ID
    orgId: string,
    //权限编码
    permissionCode: string,
    //权限名称
    permissionName: string,
    //创建时间
    createTime: string,
    //创建人
    createUser: string,
    //创建人名称
    createUserName: string,
    //修改时间
    updateTime: string,
    //修改人
    updateUser: string,
    //修改人名称
    updateUserName: string
}

/*
* 权限查询类型
* */
type PermissionQuery={
    //机构ID
    orgId?: string,
    //权限名称或者角色编码
    permissionText?: string,
    //分页
    page: PageType,
}

/*
* 权限操作类型
* */
type PermissionOperationVoType={
    //权限ID
    permissionId?: string,
    //机构ID
    orgId?: string,
    //权限编码
    permissionCode: string,
    //权限名称
    permissionName: string

}

//删除权限类型
type PermissionDeleteVoType ={
    deleteIds: Array<string>
}

/**
 * 权限Transfer类型
 */
type PermissionTransferType = {
    //权限ID
    id: string;
    //权限名称
    name: string;
    //是否可选
    disabled: boolean;
}

//岗位权限资源类型
type PermissionResourceVoType={
    //机构ID
    orgId?: string
    //权限ID
    permissionId: string
    //操作的资源
    resourceIds: Array<string>
}

export type {
    PermissionTableType,
    PermissionQuery,
    PermissionOperationVoType,
    PermissionDeleteVoType,
    PermissionTransferType,
    PermissionResourceVoType
}