//部门新增类型
type DeptOperationVoType={
    //部门ID
    deptId?: string
    //父级部门ID
    parentId?: string
    //机构ID
    orgId?: string
    //部门名称
    deptName?: string
    //部门编码
    deptCode?: string
    //部门排序
    deptOrder?: number
}
//部门删除类型
type DeleteDeptType={
    deptIds: Array<string>
}

/*
* 定义部门信息State类型
* */
type DeptTreeType={
    //部门ID
    id: string;
    //部门ID(兼容组件)
    value: string;
    //部门名称(兼容组件)
    label: string;
    //部门名称
    name: string;
    //子集部门
    children?: DeptTreeType[];
}
/*
* 部门Tree列表的类型
* */
type DeptWebVo={
    //部门ID
    deptId: string;
    //父级id
    parentId: string;
    //部门名称
    deptName: string;
    //部门全名
    deptFullName: string;
    //部门编码
    deptCode: string;
    //部门等级
    deptLv: number;
    //部门排序
    deptOrder: number;
    /**
     * 创建人信息
     */
    createUser: string;
    createUserName: string;
    createTime: string;
    /**
     * 修改人信息
     */
    updateUser: string;
    updateUserName: string;
    updateTime: string;

    //子集部门
    children?: Array<DeptWebVo>;
}

export type {
    DeptOperationVoType,
    DeleteDeptType,
    DeptTreeType,
    DeptWebVo
}