//定义菜单Tree
import type {PageType} from "@/common/base/baseType";

type MenuTree = {
    id: string
    name: string
    children?: MenuTree[]
}
//菜单vo类型
type MenuWebVo = {
    /**
     * 菜单ID
     */
    menuId: string;

    /**
     * 菜单编码
     */
    menuCode: string;

    /**
     * 菜单名称
     */
     menuName: string;

    /**
     * 资源路径
     */
     url: string;

    /**
     * 排序
     */
    sort: number;

    /**
     * 菜单图标
     */
     menuIcon: string;
}
//菜单查询资源queryVo类型
type MenuResourceQueryVo = {
    //菜单id
    menuId: string;

    /*
     * 资源名称或者资源编码
     * */
    resourceText: string;

    /**
     * 分页对象
     */
    //分页
    page: PageType,
}
//菜单资源vo类型
type MenuResourceVo = {
    //菜单资源id
    menuResourceId: string;
    //菜单id
    menuId: string;
    //资源id
    resourceId: string;
    //资源名称
    resourceName: string;
    //资源编码
    resourceCode: string;
    //资源url
    url: string;
}

//删除菜单资源类型
type MenuResourceDeleteVo ={
    menuId: string;
    deleteIds: Array<string>
}

//菜单资源vo类型
type MenuResourceBindVo={
    //菜单id
    menuId: string;
    //操作的资源
    resourceIds: Array<string>
}

//菜单资源TransferVo
type MenuTransferVo = {
    //菜单ID
    id: string;
    //菜单名称
    name: string;
    //是否可选
    disabled: boolean;
}

//角色菜单类型
type RoleMenuVo = {
    //角色id
    roleId: string;
    //菜单ids
    menuIds: Array<string>;
}

//导出类型
export type {
    MenuTree,
    MenuWebVo,
    MenuResourceQueryVo,
    MenuResourceVo,
    MenuResourceDeleteVo,
    MenuResourceBindVo,
    MenuTransferVo,
    RoleMenuVo
}