import type {PageType} from "@/common/base/baseType";

/*
* 岗位列表类型
* */
type StationType = {
    /**
     * 岗位
     */
    stationId: string;

    /**
     * 所属机构Id
     */
    orgId: string;

    /**
     * 直属部门ID
     */
    deptId: string;

    /**
     * 岗位名称
     */
    stationName: string;

    /**
     * 岗位编码
     */
    stationCode: string;

    /**
     * 机构名称
     */
    orgName: string;

    /*
    * 部门名称
    * */
    deptName: string;

    /*
    * 部门全名
    * */
     deptFullName: string;

    /**
     * 创建时间
     */
    createTime: string;

    /**
     * 创建人
     */
    createUser: string;

    /**
     * 创建人名称
     */
    createUserName: string;

    /**
     * 修改时间
     */
    updateTime: string;

    /**
     * 修改人
     */
    updateUser: string;

    /**
     * 修改人名称
     */
     updateUserName: string;

}

/**
 * 岗位Transfer类型
 */
type StationTransferType = {
    //岗位ID
    id: string;
    //岗位名称
    name: string;
    //岗位编码
    disabled: boolean;
}

/*
* 定义岗位State类型
* */
type StationStateType ={
    id: string;
    value: string;
    label: string;
    name: string;
    deptId?: string;
    deptName?: string;
    children?: StationStateType[];
}


//岗位查询类型
type StationQueryType ={
    //岗位ID
    stationId?: string
    //岗位名称
    stationName?: string
    //岗位编码
    stationCode?: string
    //所属机构
    orgId?: string
    //所属部门
    deptId?: string
    //分页对象
    page: PageType
}

/**
 * 岗位操作类型
 * 1.用于新增岗位
 * 2.用于修改岗位
 */
type StationOperationVoType ={
    //岗位ID
    stationId?: string
    //所属机构
    orgId?: string
    //所属部门
    deptId?: string
    //岗位名称
    stationName: string
    //岗位编码
    stationCode: string
}
//删除岗位类型
type DeleteStationType={
    stationIds: Array<string>
}

//用户岗位操作类型
type StationUserVoType={
    //机构ID
    orgId?: string
    //用户ID
    userId: string
    //操作的岗位ID集合
    stationIds: Array<string>
}

//岗位角色类型
type StationRoleVoType={
    //机构ID
    orgId?: string
    //岗位ID
    stationId: string
    //操作的角色
    roleIds: Array<string>
}

//岗位权限类型
type StationPermissionVoType={
    //机构ID
    orgId?: string
    //岗位ID
    stationId: string
    //操作的角色
    permissionIds: Array<string>
}

export type {
    StationType,
    StationStateType,
    StationQueryType,
    StationOperationVoType,
    DeleteStationType,
    StationTransferType,
    StationUserVoType,
    StationRoleVoType,
    StationPermissionVoType
}