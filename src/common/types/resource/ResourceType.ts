//引入分页类型
import type {PageType} from "@/common/base/baseType";

/*
* 资源列表
* */
type ResourceTableType={
    /**
     * 资源ID
     */
    resourceId: string;

    /**
     * 资源全路径
     */
    resourceFullId: string;

    /**
     * 所属机构ID
     */
    orgId: string;

    /**
     * 资源编码
     */
    resourceCode: string;

    /**
     * 资源名称
     */
    resourceName: string;

    /**
     * 资源全名
     */
    resourceFullName: string;

    /**
     * 01-接口	02-文件	03-其他
     */
     resourceType: string;

    /**
     * 父级权限
     */
    parentResourceId: string;

    /**
     * 资源路径
     */
    url: string;

    /**
     * 层级
     */
    lv: number;

    /**
     * 排序
     */
    sort: number;

    /**
     * 创建时间
     */
    createTime: string,

    /**
     * 创建人
     */
    createUser: string,

    /**
     * 创建人名称
     */
    createUserName: string,

    /**
     * 修改时间
     */
    updateTime: string,

    /**
     * 修改人
     */
    updateUser: string,

    /**
     * 修改人名称
     */
    updateUserName: string
}

/*
* 资源查询类型
* */
type ResourceQueryType={
    //机构ID
    orgId?: string,
    //资源名称或者角色编码
    resourceText?: string,
    //分页
    page: PageType,
}

/*
* 资源操作类型Type
* */
type ResourceOperationType={
    /**
     * 资源ID
     */
    resourceId?: string;

    /**
     * 所属机构ID
     */
    orgId?: string;

    /**
     * 资源编码
     */
    resourceCode: string;

    /**
     * 资源名称
     */
    resourceName: string;

    /**
     * 资源路径
     */
    url?: string;
}

//删除资源类型
type ResourceDeletVoType ={
    deleteIds: Array<string>
}

/**
 * 资源Transfer类型
 */
type ResourceTransferType = {
    //资源ID
    id: string;
    //资源名称
    name: string;
    //是否操作
    disabled: boolean;
}

export type {
    ResourceTableType,
    ResourceQueryType,
    ResourceOperationType,
    ResourceDeletVoType,
    ResourceTransferType
}