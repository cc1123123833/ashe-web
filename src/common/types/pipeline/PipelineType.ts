/*
* 流水线类型
* */
//引入分页类型
import type {PageType} from "@/common/base/baseType";

/**
 * 流水线table类型
 */
type PipelineWebVoType={
    // 流水线ID
    pipelineId: string,
    // 流水线名称
    pipelineName: string,
    // 流水线编码
    pipelineCode : string,
    // 流水线状态
    runStatus: string,
    // 流水线状态名称
    runStatusName: string,
    //运行开始时间
    runTimeStart: string,
    //运行结束时间
    runTimeEnd: string,
}

/*
* 流水线查询类型
* */
type PipelineQueryVoType={
    //当前登录人ID
    accountId: string,
    //流水线ID
    pipelineId: string,
    // 流水线名称
    pipelineName: string,
    // 流水线状态
    pipelineStatus: string,
    //分页
    page: PageType,
}
/*
* 流水线绘制类型
* */
type PipelineDrawVoType={
    //流水线ID
    pipelineId: string,
    //流水线编码
    pipelineCode: string,
    // 流水线名称
    pipelineName: string,
    // 流水线描述
    pipelineDesc: string,
    //流水线绘制json
    pipelineJson: string,
}
/*
* 流水线部署类型
* */
type PipelineDeployVoType={
    //流水线ID
    pipelineId: string,
    //流水线编码
    pipelineCode: string,
    // 流水线名称
    pipelineName: string,
    // 流水线描述
    pipelineDesc: string,
    //流水线绘制json
    pipelineJson: string,
    //当前登录人ID
    accountId: string,
    //流水线的节点数据
    taskNodes: Array<TaskNode>,
    //流水线连接线数据
    connectionLines: Array<ConnectionLine>

}
type TaskNode={
    //节点ID
    id: string,
    /**
     * 节点类型
     * start 开始节点
     * end 结束节点
     * codetask 代码任务节点
     * citask ci任务节点
     * cdtask cd任务节点
     * -----------------------
     * 以上都是 serviceTask
     */
    type: string,
    //节点名称
    label: string,
    //节点绘制对象
    dimensions: Dimensions,
    //节点坐标对象
    position: Position
}


type Dimensions={
    //节点宽度
    width: string,
    //节点高度
    height: string,
}

type Position={
    //节点X坐标
    x: string,
    //节点Y坐标
    y: string,

}

type ConnectionLine={
    //连接线id
    id: string,
    //连接线类型(默认=default)
    type: string,
    //连接线起点的节点id
    source: string,
    //连接线终点的节点id
    target: string,
    //连接线起点x坐标
    sourceX: string,
    //连接线起点y坐标
    sourceY: string,
    //连接线终点x坐标
    targetX: string,
    //连接线终点y坐标
    targetY: string,
}

export type {
    PipelineWebVoType,
    PipelineQueryVoType,
    PipelineDrawVoType,
    PipelineDeployVoType
}