import type { PageType } from "@/common/base/baseType";
/*
* 用户列表类型
* */
type UserTableType ={
    //账号ID
    accountId: string
    //账号名称
    accountName: string
    //用户名称
    name: string
    //账号状态
    accountStatusName?: string
    //直属部门
    deptFullName?: string
    //直属岗位
    stationName?: string
    //创建时间
    createTime: string
    //创建人
    createUserName: string
    //修改时间
    updateTime: string
    //修改人
    updateUserName: string
}

//用户查询类型
type UserQueryType ={
    //账号ID
    accountId?: string
    //账号名称
    accountName?: string
    //用户名称
    name?: string
    //账号状态
    accountStatusName?: string
    //所属机构
    orgId?: string
    //直属部门
    deptId?: string
    //直属岗位
    stationId?: string
    //分页对象
    page: PageType
}

//新增账号类型
type AccountType ={
    //账号ID
    accountId?: string
    //账号名称
    accountName?: string
    //用户名称
    name?: string
    //账号密码
    accountPwd?: string
    //账号状态
    accountStatusName?: string
    //所属机构
    orgId?: string
    //直属部门
    deptId?: string
    //直属岗位
    stationId?: string
    //性别
    gender?: string
    //分页对象
    enabled?: boolean
}
//删除账号类型
type DeleteAccountType ={
    accountIds: Array<string>
}

//重置密码类型
type ResetPasswordType ={
    accountId: string
    password: string
}

/**
 * 定义用户信息State类型
 */
type UserInfoType ={
    accountId?: string;
    accountName?: string;
    accountPassword?: string;
    userName?: string,
    orgId?: string,
    orgName?: string,
    deptId?: string,
    deptName?: string,
    lessee?: string,
    captcha?: string
    rememberMe?: boolean
}

/*
* 登录参数类型
* */
type LoginType ={
    username: string,
    password: string,
    captcha: string,
    tempId?: string
}

/**
 * 定义State返回状态类型
 */
type UserState ={
    userInfoState: UserInfoType,
    getUserInfo: () => UserInfoType,
    userInfoActions: () => void,
    loginActions: (loginfo: LoginType) => void,
    $reset: () => void
}
//导出类型
export type
{
    UserTableType,
    UserQueryType,
    AccountType,
    DeleteAccountType,
    ResetPasswordType,
    UserInfoType,
    UserState,
    LoginType
}