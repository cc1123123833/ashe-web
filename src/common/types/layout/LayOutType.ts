/**
 * 定义布局的state类型
 * asideWidth 侧边栏宽度
 * tabList 选项卡列表
 * chooseTab 当前选中的选项卡
 */
type LayOutType ={
    asideWidth: string,
    tabList: Array<TabType>,
    chooseTab: string
}
//选项卡类型
type TabType ={
    title: string,
    name: string
}

/**
 * 定义State状态类型
 */
type LayOutState ={
    layoutState: LayOutType
}

export type {
    LayOutType,
    LayOutState,
    TabType
}