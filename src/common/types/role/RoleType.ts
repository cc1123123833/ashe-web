/*
* 角色表格类型
* */
//引入分页类型
import type {PageType} from "@/common/base/baseType";

type RoleTableType={
    /**
     * 角色ID
     */
    roleId: string,

    /**
     * 所属机构ID
     */
    orgId: string,

    /**
     * 角色编码
     */
    roleCode: string,

    /**
     * 角色名称
     */
    roleName: string,

    /**
     * 创建时间
     */
    createTime: string,

    /**
     * 创建人
     */
    createUser: string,

    /**
     * 创建人名称
     */
    createUserName: string,

    /**
     * 修改时间
     */
    updateTime: string,

    /**
     * 修改人
     */
    updateUser: string,

    /**
     * 修改人名称
     */
    updateUserName: string
}

/*
* 角色查询类型
* */
type RoleQueryType={
    //机构ID
    orgId?: string,
    //角色名称或者角色编码
    roleText?: string,
    //分页
    page: PageType,
}

/*
* 角色操作类型Type
* */
type RoleOperationType={
    //角色ID
    roleId?: string,
    //角色编码
    roleCode: string,
    //角色名称
    roleName: string,
    //机构ID
    orgId?: string
}

//删除角色类型
type RoleDeleteVoType ={
    roleIds: Array<string>
}

/**
 * 角色Transfer类型
 */
type RoleTransferType = {
    //角色ID
    id: string;
    //角色名称
    name: string;
    //是否操作
    disabled: boolean;
}

export type {
    RoleTableType,
    RoleQueryType,
    RoleOperationType,
    RoleDeleteVoType,
    RoleTransferType
}