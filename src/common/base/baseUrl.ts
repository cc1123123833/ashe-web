import { getTempId } from "@/utils/tempIdUtils";
//基础后端网关路径
const baseUrl = "http://localhost:9527";

//返回验证码路径
const codeUrl = () => {
    return `${baseUrl}/ca/kaptcha/code.jpg?tempId=${getTempId()}&time=${new Date().getTime()}`;
}

export {
    baseUrl,
    codeUrl
}