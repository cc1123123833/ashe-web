/**
 * 统一状态码
 */
//成功状态
const SUCCESS_CODE:number = 200;
//失败状态
const ERROR_CODE:number = 500;

export {
    SUCCESS_CODE,
    ERROR_CODE
}