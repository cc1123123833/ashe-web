/**
 * 定义返回类型
 */
type ResuleType={
    code?: number,
    msg?: string,
    data?: any
}

type PageType={
    pageNumber: number,
    pageSize: number,
}

export type {
    ResuleType,
    PageType
}